
.. _commands:

Commands
========
This page contains documentation for each BuildStream command,
along with their possible options and arguments. Each command can be
invoked on the command line, where, in most cases, this will be from the
project's main directory.


----

.. The bst options e.g. bst --version, or bst --verbose etc.
.. _invoking_bst:

.. click:: buildstream._frontend:cli
   :prog: bst

.. Further description of the command goes here

----

.. the `bst init` command
.. _invoking_init:

.. click:: buildstream._frontend.cli:init
   :prog: bst init

----

.. the `bst build` command
.. _invoking_build:

.. click:: buildstream._frontend.cli:build
   :prog: bst build

----

.. _invoking_fetch:

.. click:: buildstream._frontend.cli:fetch
   :prog: bst fetch

----

.. _invoking_track:

.. click:: buildstream._frontend.cli:track
   :prog: bst track

----

.. _invoking_pull:

.. click:: buildstream._frontend.cli:pull
   :prog: bst pull

----

.. _invoking_push:

.. click:: buildstream._frontend.cli:push
   :prog: bst push

----

.. _invoking_show:

.. click:: buildstream._frontend.cli:show
   :prog: bst show

----

.. _invoking_shell:

.. click:: buildstream._frontend.cli:shell
   :prog: bst shell

----

.. _invoking_checkout:

.. click:: buildstream._frontend.cli:checkout
   :prog: bst checkout

----

.. _invoking_source_bundle:

.. click:: buildstream._frontend.cli:source_bundle
   :prog: bst source bundle

----

.. _invoking_workspace:

.. click:: buildstream._frontend.cli:workspace
   :prog: bst workspace

----

.. _invoking_workspace_open:

.. click:: buildstream._frontend.cli:workspace_open
   :prog: bst workspace open

----

.. _invoking_workspace_close:

.. click:: buildstream._frontend.cli:workspace_close
   :prog: bst workspace close

----

.. _invoking_workspace_reset:

.. click:: buildstream._frontend.cli:workspace_reset
   :prog: bst workspace reset

----

.. _invoking_workspace_list:

.. click:: buildstream._frontend.cli:workspace_list
   :prog: bst workspace list
